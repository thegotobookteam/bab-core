package com.stgconsulting.bab.util;

import com.stgconsulting.bab.exception.common.ParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * A Json Utility.
 */
public class JsonUtil {
    /**
     * Parse JSON to pClass object.
     *
     * @param pBody  the {@link String} Http response body (JSON).
     * @param pClass the {@link Class} class to parse
     * @param <T>    the {@link Class} return object
     * @return the {@link Class} object
     */
    public static <T> T mapFromJson(String pBody, Class pClass) throws ParseException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            return mapper.reader().forType(pClass).readValue(pBody);
        } catch (IOException e) {
            throw new ParseException(e, pBody, pClass);
        }
    }
}
