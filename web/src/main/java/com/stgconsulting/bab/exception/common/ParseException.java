package com.stgconsulting.bab.exception.common;

import lombok.Data;

import java.io.IOException;

/**
 * Parsing Exception.
 */
@Data
public class ParseException extends IOException {
    private String body;
    private Class resultClass;

    public ParseException(IOException e, String body, Class resultClass ) {
        super(e);
        this.body = body;
        this.resultClass = resultClass;
    }
}
