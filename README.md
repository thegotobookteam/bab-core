# bab-core

This is a library of the core functionality required for the Build-A-Burger or BAB micro-services.

## Getting Started

Simply add the appropriate Maven dependencies for the library version you wish to use. The `LOCAL` is the `buildNumber` and by default has a value of `LOCAL`, this value can be set during the automated build service, i.e. bamboo, jenkins, etc. 
```
<properties>
    <bab.core.version>1.LOCAL</bab.core.version>
</properties>

<dependency>
    <groupId>com.stgconsulting.bab</groupId>
    <artifactId>bab-core-web</artifactId>
    <version>${bab.core.version}</version>
</dependency>
<dependency>
    <groupId>com.stgconsulting.bab</groupId>
    <artifactId>bab-core-model</artifactId>
    <version>${bab.core.version}</version>
</dependency>
```

The `<bab.core.version>` versions are available in our artifactory (currently does not exist) 
[here](https://stgconsulting.com/artifactory/stg-release/com/stgconsulting/apps/bab/bab-core-web/).   

**Feature Introduction Table**

| Feature | Version | Description |
|:--------|:--------|:------------|
| @NoArgConstructors | 1.LOCAL | [Lombok](https://projectlombok.org) `@NoArgConstructor` added to model objects. (example) | 

## Authors

* **David Q. Romney** - [Software Technology Group](https://bitbucket.org/%7B373594fb-52bc-43ea-a96b-75962dc24892%7D/)
